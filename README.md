# SleepyPen

A simple utility made with WinUI 3 for SlimPen users to quickly access basic power options.

# Usage

Set a pen shortcut to launch Sleepy Pen, and there you go!