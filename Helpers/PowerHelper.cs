﻿using System.Diagnostics;
using System.Runtime.InteropServices;

namespace SleepyPen.Helpers
{
    internal static class PowerHelper
    {
        [DllImport("PowrProf.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        public static extern bool SetSuspendState(bool hiberate, bool forceCritical, bool disableWakeEvent);

        public static void PowerOff() => Process.Start("shutdown", "/s /t 0");

        public static void Sleep() => SetSuspendState(false, true, true);

        public static void Hibernate() => SetSuspendState(true, true, true);
    }
}
