using Microsoft.UI;
using Microsoft.UI.Windowing;
using Microsoft.UI.Xaml;
using Microsoft.UI.Xaml.Controls;
using Microsoft.UI.Xaml.Controls.Primitives;
using Microsoft.UI.Xaml.Data;
using Microsoft.UI.Xaml.Input;
using Microsoft.UI.Xaml.Media;
using Microsoft.UI.Xaml.Navigation;
using SleepyPen.Helpers;
using SleepyPen.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;

// To learn more about WinUI, the WinUI project structure,
// and more about our project templates, see: http://aka.ms/winui-project-info.

namespace SleepyPen
{
    /// <summary>
    /// An empty window that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainWindow : Window
    {
        private readonly object _locker = new();
        private bool hasActed = false;

        private MainWindowViewModel ViewModel => MainPanel.DataContext as MainWindowViewModel;

        public MainWindow()
        {
            this.InitializeComponent();
            ExtendsContentIntoTitleBar = true;
            GetAppWindowForCurrentWindow().SetPresenter(AppWindowPresenterKind.FullScreen);
            CountDownAsync();
        }

        private AppWindow GetAppWindowForCurrentWindow()
        {
            IntPtr hWnd = WinRT.Interop.WindowNative.GetWindowHandle(this);
            WindowId myWndId = Win32Interop.GetWindowIdFromWindow(hWnd);
            return AppWindow.GetFromWindowId(myWndId);
        }

        private void ActionOnce(Action action)
        {
            lock (_locker)
            {
                if (hasActed) return;
                hasActed = true;
            }
            action();
            Environment.Exit(0);
        }

        private async void CountDownAsync()
        {
            do
            {
                await Task.Delay(1000);
                ViewModel.Timer--;
            } while (ViewModel.Timer > 0);
            ActionOnce(PowerHelper.Sleep);
        }

        private void Sleep(object sender, RoutedEventArgs e)
        {
            ActionOnce(PowerHelper.Sleep);
        }

        private void Hibernate(object sender, RoutedEventArgs e)
        {
            ActionOnce(PowerHelper.Hibernate);
        }

        private void PowerOff(object sender, RoutedEventArgs e)
        {
            ActionOnce(PowerHelper.PowerOff);
        }

        private void Cancel(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
