﻿using CommunityToolkit.Mvvm.ComponentModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SleepyPen.ViewModels
{
    internal class MainWindowViewModel : ObservableObject
    {
        private int timer = 15;
        public int Timer { get => timer; set => SetProperty(ref timer, value); }
    }
}
